---
title : "Fictieve belastingdienst and waterschap frontend"
description: "Frontend to visualize data in registries that the tax and water authorities have access to, scoped to the WOZ."
date: 2023-10-27T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Clone this repo. Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go
```

This starts a web server locally on the address specified in the config (see config/dev.yaml).


## Backend(s)

This repository expects that the Belastingsamenwerking backend is accessible, the endpoints of which can be configured using the config.


### Hot reload

To enable hot reload when e.g. a view (template) changes, install a tool like [entr](https://github.com/eradman/entr) and run e.g.
```
find views/ | entr -r go run main.go
```
