package main

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingdienst-and-waterschap-frontend/backend"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingdienst-and-waterschap-frontend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingdienst-and-waterschap-frontend/helpers"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-belastingdienst-and-waterschap-frontend/model"
)

func main() {
	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"configOrganization": func() interface{} { return config.Config.Organization },
		"numberFormat":       helpers.NumberFormat,
		"formatDate":         helpers.FormatDate,
		"formatYear":         helpers.FormatYear,
		"addInt":             helpers.Add[int], // IMPROVE: define in a generic way? See e.g. https://github.com/gohugoio/hugo/blob/master/tpl/math/math.go
		"subInt":             helpers.Sub[int],
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",

		// Override default error handler
		ErrorHandler: func(c *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			// Send custom error page
			if err = c.Render("error", fiber.Map{
				"title":   fmt.Sprintf("Fout %d", code),
				"code":    code,
				"details": err.Error(),
			}); err != nil {
				// In case serving the error page the fails
				return c.Status(code).SendString(err.Error())
			}

			// Return from handler
			return nil
		},
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// Tax assessments
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the tax assessments from the Tax assessments backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10")) // IMPROVE: use implicitly, do not include in query parameters?
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.TaxAssessment]
		if err := backend.Request(http.MethodGet, config.Config.Belastingsamenwerking.Domain, fmt.Sprintf("/assessments?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Belastingsamenwerking.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching tax assessments: %v", err)
		}

		return c.Render("index", fiber.Map{
			"title":       "OZB-aanslagen",
			"assessments": response.Data,
			"pagination":  response.Pagination,
		})
	})

	assessments := app.Group("/assessments")
	// assessments.Get("/:id", func(c *fiber.Ctx) error {
	// 	// Fetch the tax assessment from the Tax assessments backend
	// 	assessmentID, err := uuid.Parse(c.Params("id"))
	// 	if err != nil {
	// 		return fmt.Errorf("uuid parse failed: %w", err)
	// 	}

	// 	assessment := new(model.TaxAssessment)
	// 	if err := backend.Request(http.MethodGet, config.Config.Belastingsamenwerking.Domain, fmt.Sprintf("/assessments/%s", assessmentID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.Belastingsamenwerking.FSCGrantHash}, assessment); err != nil {
	// 		return fmt.Errorf("tax assessment request failed: %w", err)
	// 	}

	// 	return c.Render("woz-assessment-details", fiber.Map{
	// 		"title": "",
	// 		"assessment": assessment,
	// 	})
	// })

	assessments.Get("/:id/download", func(c *fiber.Ctx) error {
		// Fetch the tax assessment from the Tax assessments backend
		assessmentID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		// Note: We cannot use a byte stream to download the PDF directly into the ResponseWriter
		// Because this is not support by fiber
		file, err := os.CreateTemp("/tmp/", "temp-*.pdf")
		if err != nil {
			return nil
		}

		defer os.Remove(file.Name())

		if err := backend.Download(http.MethodGet, config.Config.Belastingsamenwerking.Domain, fmt.Sprintf("/assessments/%s/download", assessmentID), map[string]string{"Fsc-Grant-Hash": config.Config.Belastingsamenwerking.FSCGrantHash}, file); err != nil {
			return fmt.Errorf("tax assessment download request failed: %w", err)
		}

		return c.Download(file.Name(), fmt.Sprintf("aanslagbiljet-%s.pdf", time.Now().Format("2006-01-02T15:04:05")))
	})

	// Start server
	if err := app.Listen(config.Config.ListenAddress); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
